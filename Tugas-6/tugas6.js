// SOAL 1


function hitung(r)
{
    this.r = r;
  // luas
    this.luas = function () 
    {
        return Math.PI * this.r * this.r;
    };
  // keliling
    this.keliling = function ()
    {
        return 2*Math.PI*this.r;
    };
}
let lingkaran = new hitung(7);
console.log("Luas lingkaran = " 
+ lingkaran.luas().toFixed(2));


console.log("Keliling lingkaran = " + lingkaran.keliling().toFixed(2));




// SOAL 2


let kalimat = "";


function sentence(kalimat) {
  return `saya adalah seorang ${kerja}`;


}
let kerja = "frontend developer";
console.log(sentence());




// SOAL 3


const newFunction = function literal(firstName, lastName){
  return {
    fullName: function(){
      console.log(`${firstName} ${lastName}`)
      return 
    }
  }
}


//Driver Code 
newFunction("William", "Imoh").fullName();




// SOAL 4


const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}


const {firstName, lastName, destination, occupation, spell} = newObject;


console.log(firstName, lastName, destination, occupation);




// SOAL 5


const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]


//Driver Code
console.log(combined);