// SOAL 1

function halo() {
    return "Halo Sanbers!";
  }
  
  console.log(halo());
  
  
  // SOAL 2
  
  function kalikan(num1, num2) {
    return num1 * num2;
  }
  
  var num1 = 12;
  var num2 = 4;
  var hasilKali = kalikan(num1, num2);
  console.log(hasilKali);
  
  
  // SOAL 3
  
  function introduce(name, age, address, hobby) {
        return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
  }
  
  var name = "John";
  var age = 30;
  var address = "Jalan belum jadi";
  var hobby = "Gaming";
  var perkenalan = 
  introduce(name, age, address, hobby);
  console.log(perkenalan);
  
  
  // SOAL 4
  
  var arrayDaftarPeserta = {
    name : "Asep" ,
    gender : "Laki-laki" ,
    hobby : "baca buku" ,
    tahunlahir : 1992 ,
  };
  
  var daftarPeserta = 
    Object.keys(arrayDaftarPeserta);
  
  for (a = 0; 
    a < daftarPeserta.length; 
    a++) 
  {
  console.log(arrayDaftarPeserta[daftarPeserta[a]]);
  }
  
  
  // SOAL 5
  
  var dataBuah = [
        {1 :
         {nama : "strawberry" ,
          warna : "merah" ,
          ada_bijinya : "tidak" ,
          harga : "9000"}
         },
        {2 : 
          {nama : "jeruk" ,
          warna : "oranye" ,
          ada_bijinya : "ada" ,
          harga : "8000"}
        },
        {3 : {nama : "Semangka" ,
          warna : "Hijau & Merah" ,
          ada_bijinya : "ada" ,
          harga : "10000"}
        },
        {4 : {nama : "Pisang" ,
          warna : "Kuning" , 
          ada_bijinya : "tidak" ,
          harga : "5000"}
         }
  ]
  console.log(dataBuah[0]);
  
  
  // SOAL 6
  
  var dataFilm = []
  
  function film(nama, durasi, genre,tahun) {
  dataFilm.push(nama, durasi, genre, tahun)
  }
  film("Goodbye days",  "1 jam 36 menit",  "romance", "tahun 2019")
  console.log(dataFilm);
  
  