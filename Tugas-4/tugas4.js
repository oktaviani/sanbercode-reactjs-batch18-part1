// SOAL 1

var jawaban1 = ""
var a = 1;
var b = 20;
var loop1 = "";
var loop2 = "";

while(a <= 20) {
    if(a % 2 == 0) {
        loop1 += [a] + " - I love coding \n";
    }
    a++;
}

while (b >= 2) {
    if(b % 2 == 0) {
        loop2 += [b] + " - I will become a frontend developer \n";
    }
    b--;
}

jawaban1 = "LOPPING PERTAMA \n" + loop1 + "LOPING KEDUA \n" + loop2;

console.log(jawaban1);


// SOAL 2

var jawaban2 = ""
        
for (var c = 1; c <= 20; c++) {
    if (c % 2 == 1) {
        if (c % 3 == 0) {
            jawaban2 += [c] + " - I Love Coding \n";
        } else {
            jawaban2 += [c] + " - Santai \n";
        }
    } else {
        jawaban2 += [c] + " - Berkualitas \n";
    }
}
    
console.log(jawaban2);


// SOAL 3

function buatBaris(length) {
  for (var baris="", j=1; j <= length; j++)
    baris += j === length ? "#" : "# ";
  return baris + "\n";
}

function pagar(length) {
  for (var space="", i=1; i<=length; i++)
    space += buatBaris(i);
  return space;
}

console.log(pagar(7));


// SOAL 4

var kalimat = "saya sangat senang belajar javascript";
var kata = kalimat.split(" ");
console.log(kata);


// SOAL 5

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
buah = daftarBuah.sort();
for(var i = 0; i < buah; i++) {
    console.log(buah[i]);
}
