// SOAL 1
var kataPertama = "saya ";
var kataKedua = "senang ";
var kataKetiga = "belajar ";
var kataKeempat = "javascript";
var kataKeduaBesar = kataKedua.slice(0, 1).toUpperCase();
var kataKeduaKecil = kataKedua.slice(1);
var gabungKataKedua = kataKeduaBesar.concat(kataKeduaKecil);
var gabungKalimat = kataPertama.concat(gabungKataKedua + kataKetiga +
kataKeempat.toUpperCase());
console.log(gabungKalimat);


// SOAL 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
angka1 = parseInt(kataPertama);
angka2 = parseInt(kataKedua);
angka3 = parseInt(kataKetiga);
angka4 = parseInt(kataKeempat);
jumlah = angka1 + angka2 + angka3 + angka4;
console.log(jumlah);


// SOAL 3
var kalimat = 'wow javascript itu keren sekali';
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 30);

console.log('Kata Pertama : ' + kataPertama);
console.log('Kata Kedua : ' + kataKedua);
console.log('Kata Ketiga : ' + kataKetiga);
console.log('Kata Keempat : ' + kataKeempat);
console.log('Kata Kelima : ' + kataKelima);


// SOAL 4
var nilai = 85;
if (nilai >= 80 && nilai <= 100) {
    console.log("A");
} else if (nilai >= 70 && nilai < 80) {
    console.log("B");
} else if (nilai >= 60 && nilai < 70) {
    console.log("C");
} else if (nilai >= 50 && nilai < 60) {
    console.log("D");
} else if (nilai < 50) {
    console.log("E");
}
console.log(nilai);


// SOAL 5
var tanggal = 11;
var bulan = 10;
var tahun = 2000;
var stringBulan = "";

switch (bulan) {
case 1 : stringBulan = " Januari "; break;
case 2 : stringBulan = " Februari "; break;
case 3 : stringBulan = " Maret "; break;
case 4 : stringBulan = " April "; break;
case 5 : stringBulan = " Mei "; break;
case 6 : stringBulan = " Juni "; break;
case 7 : stringBulan = " Juli "; break;
case 8 : stringBulan = " Agustus "; break;
case 9 : stringBulan = " September "; break;
case 10 : stringBulan = " Oktober "; break;
case 11 : stringBulan = " November "; break;
case 12 : stringBulan = " Desember "; break;
}

var tampil = tanggal + stringBulan + tahun;
console.log(tampil);



